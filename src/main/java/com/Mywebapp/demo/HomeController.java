package com.Mywebapp.demo;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller

public class HomeController {
	
	private static int id=1;
	
	@GetMapping("/")
	public String getForm() {
        return "home";
    }
	
	@PostMapping("/saveDetails")                    
    public String saveDetails(@RequestParam("firstname") String firstName,
                              @RequestParam("lastname") String lastName,
                              @RequestParam("dateofbirth") String dateofbirth,
                              ModelMap modelMap) {

        modelMap.put("firstName", firstName);
        modelMap.put("lastName", lastName);
        modelMap.put("dateofbirth", dateofbirth);
        modelMap.put("id", id);
        id++;
        return "result";          
    }
}