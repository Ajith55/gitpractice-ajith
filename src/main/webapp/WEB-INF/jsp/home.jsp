<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html ng-app>

  <head>
    <title>Student Registration Form</title>
    <meta charset="utf-8" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    
    
  </head>
   
    
  
<body>

<div class="container">
<br>  
<hr>

<div class="row justify-content-center">
<div class="col-md-6">
<div class="card">
<header class="card-header">
  
  <h4 class="card-title mt-2">Register Here</h4>
</header>
<article class="card-body">

<form method="post" action="saveDetails">


  <div class="form-row">
    <div class="col form-group">
      <label for="validationDefault01">First name </label>   
        <input type="text" name="firstname" class="form-control" id="validationDefault01" placeholder="First Name" required>
    </div> 

    <div class="col form-group">
      <label for="validationDefault01">Last name</label>
        <input type="text" name="lastname" class="form-control" id="validationDefault01" placeholder="Last Name" required>
    </div>




    <div class="col form-group">
      <label for="validationDefault01">Date of Birth</label>
        <input type="text" name="dateofbirth" id="datepicker" class="datepicker form-control" id="validationDefault01" placeholder="MM/DD/YYYY" required>
    </div>

  </div> 



    <div class="form-group">
        <button type="submit" value="submit" class="btn btn-primary btn-block"> Register  </button>
    </div>  
  </div>                                           
</form>


</article>


   </div>
   </div>
   </div>
   </div>

    
    <script>

        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });

        (function() {
  'use strict';
  window.addEventListener('load', function() {
    var forms = document.getElementsByClassName('needs-validation');
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();


       
        

    </script>    
   
    
  </body>

</html>
